public class ProductDB {

    /*
    7-1 step 11: made getProduct regular, not static
    */
    public Product getProduct(String productCode) {
        // In a more realistic application, this code would
        // get the data for the product from a file or database
        // For now, this code just uses if/else statements
        // to return the correct product data

        // create the Product object
        Product product = new Product();

        // fill the Product object with data
        //product.setCode(productCode);
        /*
        7-1 step 6: added new product to each if/else. 
        also made data work off new constructor instead of set methods
        */
        if (productCode.equalsIgnoreCase("java")) {
            product = new Product(productCode, "Murach's Java Programming", 57.50);
        } else if (productCode.equalsIgnoreCase("jsp")) {
            product = new Product(productCode, "Murach's Java Servlets and JSP", 57.50);
        } else if (productCode.equalsIgnoreCase("mysql")) {
            product = new Product(productCode, "Murach's MySQL", 54.50);
            /*
            7-1 step 3 added a new product
            */
        } else if (productCode.equalsIgnoreCase("newBook")) {
            product = new Product(productCode, "this is a new book", 100.00);
        }
        else {
            product.setDescription("Unknown");
        }
        return product;
    }
}